# Nodevember2022

## Description

Hello and welcome in my Nodevember2022 challenge repo!

You will find here the `.blend` files of my submissions of the month.
Feel free to download them and have all the fun with it.

I am fully conscious that the nodes can be widely improved, given my very little experience and the short amount of time to produce them.
Any proposition or constructive feedback is most welcome.

Until I set up a mean for you to propose changes directly on the repo, you can contact me through one of the following channels:

- twitter: [@AlexandreFiotti](https://twitter.com/AlexandreFiotti)
- instagram: [alex_ftti](https://www.instagram.com/alex_ftti/)

## How to download the .blend

Open the "prompts" folder by clicking on it:

![Open the "prompts" folder by clicking on it](./includes/nodevember_repo_init.png 'Open the "prompts" folder')

Go to the desired prompt:

![Go to the desired prompt](./includes/nodevember_repo_prompt_choose.png 'Go to the desired prompt')

Click on the file:

![Click on the file](./includes/file_choice.png 'Click on the file')

Download:

![Download](./includes/download.png 'Download')

## Contributing

cf. `./CONTRIBUTING.md`

## Licence

[Creative Commons Attribution-ShareAlike 4.0 International Public (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/)